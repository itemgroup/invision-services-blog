---
layout: post
title: "Is investing bad during 2020?
 Read again and you'll find your solution."
date: 2020-11-16
description:
image: /assets/images/placeholder-16.jpg
author: Renato Lerner
tags:
  - Storytelling
---
Investing in real estate in 2020 might seem like a crazy decision while in the middle of a pandemic. But we know better than that: some states are in fact growing business opportunities amid the world-wide economic crisis, and for certain people, the chances are just perfect to invest.

 Our customers know this, that's why recently we had a conversation that helps us see that indeed, the challenges create unique scenarios. One of our recurring investors decided that he wanted another investment project that we could develop for him: one that could help put a profitable margin on top of an existing property. He wanted a full restyling service that could help close a sale on a property he wanted to put out in the market.

 The process itself was easy: with all the security measures in place, our professionals got to work. With small, but meaningful changes the property became an oasis in an urban area, where peace and quiet are essential to live a happy life. And in 2020, spaces with great energy are what buyers are looking for.

 With the process completed and the house on the market, there was only one thing to do: enjoy the rise in appraisal, and the profit margin that's about to be closed down in the sale of the property.
