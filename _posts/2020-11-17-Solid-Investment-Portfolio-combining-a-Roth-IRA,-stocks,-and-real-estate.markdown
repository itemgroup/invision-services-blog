---
layout: post
title: "Solid Investment Portfolio: combining a Roth IRA, stocks, and real estate!"
date: 2020-11-17
description:
image: /assets/images/placeholder-17.jpg
author: Renato Lerner
tags:
  - Investment tips
---
Investment portfolios are the best way to get and grow your wealth, smartly. To get to a point where they are not only profitable, but effectively working in your benefit, you have to build them as solid as possible with assets that not only generate money, but that gain significant value over time.

 A solid investment portfolio is composed by several assets that build up. Bonds and stocks are usually the way in which investors start theirs, and whether that is by using a Roth IRA, through stock brokers or using brokerage apps, getting a variable amount of those allows them to get results. If you're employed, then your 401(k) is a great backup plan aside from regular stock and bonds, and you can adjust it as you will to help save up for your retirement, allowing you financial freedom in other areas of your investment plan.

 Another way in which a solid investment portafolio soars is through real estate. However, this doesn't mean buying a property outright, you can add capital to a development project and get your returns effectively, even allowing you to purchase at a better price point, increasing your margin of profit.

 A great way to build and maintain a solid investment portafolio goes beyond buying and selling at the right time, it requires you to pay attention to the signs and work with professionals that know how to help you gain financial freedom and wealth.

 Contact us to know more about investment opportunities in real estate.
