---
layout: post
title: "Offshore Investment: Why Florida is your best option!"
date: 2020-11-20
description:
image: /assets/images/placeholder-20.jpg
author: Renato Lerner
tags:
  - Real Estate info
---
Florida is, without a doubt, one of the best places to invest in 2020. And for offshore investors, this is one of the reasons why they're settling their sights on the sunny state's real estate market.

 Offering any investor a friendly task environment (ranking #1 in the south side of the country), and prime locations with fantastic views and quality craftsmanship, Florida is a heaven for anyone wanting to secure their investments. Property value goes up up to 3.9% in the state, and has been doing so for the past few years. This, along with a median value of up to $230,000 per property offer a great investment opportunity for foreign investors. Florida also offers a great vacancy rate (4.5% for the better part of 2019), making real estate an active market to put money into, as it's constantly in demand.

 Are you interested in making an investment? Contact us to know the best route to invest in the US from anywhere around the world.
