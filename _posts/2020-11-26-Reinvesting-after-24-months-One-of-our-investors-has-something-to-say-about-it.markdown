---
layout: post
title: "Reinvesting after 24 months?
 One of our investors has something to say about it."
date: 2020-11-26
description:
image: /assets/images/placeholder-26.jpg
author: Renato Lerner
tags:
  - Storytelling
---
Investors know the best time to put their money to work when it comes to business. And in some cases, they know exactly how to make things work their way. We know it first hand: one of our investors decided not only to do it with us, but let us know early on he was going to reinvest with us.

 For Guillermo H*., the decision was simple: he knew that we were working on other projects besides the one where he invested, and he wanted to be a part of that process too. After all, he'd seen the results: a few weeks before he had gotten his return investment after 24 months with a large development. Guillermo can attest to the nature of our projects "Investing with Invision is so straightforward that it was simple question: did I want to let my money stagnate, or did I want it to grow steadily?". When he chose one of our developments to reinvest again, we knew he wanted one thing: for the process to be as smooth as the first time, with similar results.

 Now Guillermo is receiving his monthly reports and seeing how his first investment is not only growing, but is giving him the opportunity to purchase his dream property to renovate it with us. As a seasoned investor, he knows the value of what we offer: "It's a fantastic way to grow wealth without having to shoulder on doing it myself".

 Guillermo's story is not the only one, our investors know the quality service we provide, and how we potentiate the initial investment to create new opportunities for them to get financial wealth in a strong real estate project.

 Do you want your money to work for you? Contact us now.
 *Names were changed for privacy reasons.
