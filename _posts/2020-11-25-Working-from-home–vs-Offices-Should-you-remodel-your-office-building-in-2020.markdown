---
layout: post
title: "Working from home vs Offices
 Should you remodel your office building in 2020?"
date: 2020-11-25
description:
image: /assets/images/placeholder-25.jpg
author: Renato Lerner
tags:
  - Real Estate info
---
The COVID-19 pandemic put the world on hold: all industries had to stop and take a step back to work their way into the new normal. And soon, office buildings are going to follow suit to fit the needs for a unique set of circumstances such as the ones provided by 2020.

 Office building distributions need to be reconsidered, and for one, personal space is going to play a significant role in these designs.That's when remodeling takes center stage: with the proper modifications, millions of companies working from home could consider trickling back to work into physical establishments.

 The future of this plan working depends on two things: revision of existing systems, and adaptations of new systems through thorough remodeling plans. HVAC and general outline of the space would be addressed first: guaranteeing proper ventilation, risks of contagion reduce significantly.

 These renovations could address other problems pre-dating the pandemic, such as ergonomic distribution, and personal working stations. As such, the question is not whether to remodel or not, is when it's going to happen in order to make the transition back into the offices more effective to every part of the workforce.

 Don't miss out on the opportunity to get the best assistance for your business remodeling project. Contact our team for more information
